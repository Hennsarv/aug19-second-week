﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtensionidHulgad
{
    class Program
    {
        static void Main(string[] args)
        {

            #region Esimene osa
            //Test t = new Test("Katse");
            //Console.WriteLine(t);

            //t.M();
            //Test.M(t);

            //Test.M(E.MM(Test.M(E.MM(t))));
            //t.MM().M().MM().M().MM();

            //Console.Write("mida tahad: ");
            //Console.WriteLine(Console.ReadLine().ToProper().ToTest());

            //Console.Write("Anna andmed (nimi perenimi vanus)");
            //Console.WriteLine(Console.ReadLine().ToInimene()); 
            #endregion

            #region Teine osa
            //Console.WriteLine();

            //int seitse = 7;
            //Console.WriteLine(E.KorrutaKahega(seitse));

            //Func<int,int> kka = E.KorrutaKahega;
            //Console.WriteLine(kka(seitse));
            //kka = E.KorrutaKolmega;
            //Console.WriteLine(kka(seitse));

            //kka = (int x) => x * x;
            //Console.WriteLine(kka(seitse)); 
            #endregion

            List<int> arvud = new List<int> { 1, 7, 2, 6, 4, 2, 5, 9, 3, 5, 1 };
            Console.WriteLine(arvud.Where(x => x % 2 == 1).Sum());

            foreach (
                var x in arvud
                .Where(x => x % 3 == 0)
                //.Where(x => x < 5)
                .OrderBy(x => x)
                .Reverse()
                )
            {
                Console.WriteLine(x);
            }

        }
    }

    static class E
    {

        public static int KorrutaKahega(int x) { return x * 2; }
        public static int KorrutaKolmega(int x) { return x * 3; }

        public static int Summa(this IEnumerable<int> m)
        {
            int s = 0;
            foreach (var x in m) s += x;
            return s;
        }

        public static IEnumerable<int> Paaris(this IEnumerable<int> m)
        {
            foreach (var x in m)
            {
                if (x % 2 == 0) yield return x;
            }
        }

        public static IEnumerable<int> Milllised(this IEnumerable<int> collection, Func<int, bool> pred)
        {
            foreach (var x in collection) if (pred(x)) yield return x;
        }

        public static Test MM(this Test t)
        {
            Console.WriteLine("ma teen nüüd midagi muud asjaga " + t.Nimi);
            return t;
        }

        public static Test ToTest(this string nimi) => new Test(nimi);

        public static Inimene ToInimene(this string andmed)
        {
            return new Inimene
            {
                Eesnimi = andmed.Split(' ')[0],
                Perenimi = andmed.Split(' ')[1],
                Vanus = andmed.Split(' ')[2].ToInt(),
            };
        }

        public static int ToInt(this string x) => int.Parse(x);

        public static string ToProper(this string s)
        {
            var x = s.Split(' ');
            for (int i = 0; i < x.Length; i++)
                x[i] = x[i].Substring(0, 1).ToUpper() + x[i].Substring(1).ToLower();
            return string.Join(" ", x);
        }

        public static string ToProper2(this string s)
            => s.Split(' ')
                .Where(x => x != "")
                .Select(x => x.Substring(0, 1).ToUpper() + x.Substring(1).ToLower())
                .MyJoin(" ");

        public static string MyJoin(this IEnumerable<string> m, string s) => string.Join(s, m);

    }

    class Test
    {
        public string Nimi;                    // field
        public Test(string nimi) => Nimi = nimi;        // konstruktor
        public override string ToString() => $"Test: {Nimi}";    // overrided string

        public Test M()
        {
            Console.WriteLine("Teen midagi asjaga " + Nimi);
            return this;
        }

        public static Test M(Test t)
        {
            Console.WriteLine("Teen midagi asjaga " + t.Nimi);
            return t;
        }


    }

    class Inimene
    {
        public string Eesnimi;
        public string Perenimi;
        public int Vanus;

        public override string ToString()
        {
            return $"{Eesnimi} {Perenimi} on {Vanus} aastane";
        }
    }
}
