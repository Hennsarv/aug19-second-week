﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeelYksVahepala
{
    class Program
    {
        static void Main(string[] args)
        {
            // kui miski läheb valesti



            try
            {
                Console.WriteLine("anna üks arv: ");
                int yks = int.Parse(Console.ReadLine());
                Console.WriteLine("anna teine arv: ");
                int teine = int.Parse(Console.ReadLine());
                Console.WriteLine($"jagades {yks} / {teine} saame {yks / teine} üle jääb {yks % teine}");

            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("nulliga jagada ei saa");
            }
            catch (FormatException e)
            {
                Console.WriteLine("sa õpi arvusid sisestama");
            }
            catch (Exception e)
            {

                Console.WriteLine("miskit muud läks valesti");
                Console.WriteLine(e.Message);
            }
            finally
            {
                Console.WriteLine("ülejäänud viime poodi tagasi");
            }

            Console.WriteLine((int)"0"[0]);
            Console.WriteLine(Sünnikuupäev("35503070211")); // äge - isegi toimib!
            Console.WriteLine(Vanus("35503070211"));
            Console.WriteLine(Sugu("35503070211"));

            Inimene henn = new Inimene
            {
                IK = "35503070211",
                Eesnimi = "Henn",
                Perenimi = "Sarv"
            };

            Console.WriteLine(henn.Täisnimi);
            Console.WriteLine(henn.Fullname());
            Console.WriteLine(henn.Sugu);         // siin ma pöördun INSTANTSI funktsiooni poole
            Console.WriteLine(Inimene.Gender(henn));  // siin ma pöördun STATIC funktsioonii poole
        }

        // tegin näidiseks neli funktsiooni (AGA ÄRA TEE SAMAMOODI)
        // mis arvutavad isikukoodis soo, vanuse ja sünnipäeva
        // funktsioon aasta - see on Hennu isiklik lõbu, normaalsed inimesed nii ei te
        static int Aasta(string ik) => ((ik[0] - 1) / 2 - 6) * 100 + int.Parse(ik.Substring(1, 2));
        static DateTime Sünnikuupäev(string ik) => new DateTime(Aasta(ik), int.Parse(ik.Substring(3, 2)), int.Parse(ik.Substring(5, 2)));
        static int Vanus(string ik) => (DateTime.Today - Sünnikuupäev(ik)).Days * 4 / 1461;
        static Sugu Sugu(string ik) => (Sugu)(ik[0] % 2);

        // vot nii kirjutaks õpetatud ja hästikasvatatud programmeerija
        static int NormaalseInimeseAasta(string ik)
        {
            int aastaosa = int.Parse(ik.Substring(1, 2));
            switch (ik.Substring(0,1))
            {
                case "1": case "2": return aastaosa + 1800;
                case "3": case "4": return aastaosa + 1900;
                case "5": case "6": return aastaosa + 2000;

            }
            throw new Exception("isikukoodi esimene märk on vigane");
        }

        static Sugu NormaalseInimeseSugu(string ik)
        {
            switch (ik.Substring(0,1))
            {
                case "1": case "3": case "5": return VeelYksVahepala.Sugu.Mees;
                case "2": case "4": case "6": return VeelYksVahepala.Sugu.Naine;
            }
            throw new Exception("isikukoodi esimene märk on vale");
        }
    }

    public enum Sugu { Naine, Mees}

    class Inimene
    {
        public string Eesnimi;
        public string Perenimi;

        string _IK;
        public string IK
        {
            get => _IK;
            set
            {
                if (value.Length != 11) throw new Exception("Isikukoodi pikkus vigane");
                _IK = value;
            }
        }

        // teeme read-only property Täisnimi
        // teeme funktsiooni Fullname

        public string Täisnimi => $"{Eesnimi} {Perenimi}";  // lühikene õüleskirjutus, mis tähendab sedasama,
        // mis allpool
        //{
        //    get
        //    {
        //        return $"{Eesnimi} {Perenimi}";
        //    }
        //}

        public string Fullname() => $"{Eesnimi} {Perenimi}"; // lühikewne kirjaviis
        //{
        //    return $"{Eesnimi} {Perenimi}";
        //}

        // kuidas saada isikukoodist kuupäev kätte?
        // isikukood    cyymmddxxxz
        //              c   - sajand ja sugu
        //              yy  - sünniaasta 2 viimast nr-t
        //              mm  - sünnikuu numbrina
        //              dd  - sünnikuupäev numbrina
        //              xxx - miski number, mis meid ei huvita hetkel
        //              z   - kontrolljärk, mis meid ka ei huvita hetkel

        //      sugu = c % 2; // 1-3-5 mees 2-4-6 naine
        //      aasta = ((isikukood[0]-1)/2 - 6)*100 + int.Parse(isikukood.Substring(1,2))
        //      // mis kuradi valem see on? - lihtsamon  teha if lausega või switchiga
        //      // NB! sa ei pea niimoodi tegema - Henn siin veiderdab

        public int Vanus  => (DateTime.Today - Sünnikuupäev).Days * 4 / 1461;
        //{
        //    // teeb misngi arvutuse isikukoodiga
        //    // get => (DateTime.Today - Sünnikuupäev()).Days * 4 / 1461;
        //    // returnib tulemuse
        //}

        public DateTime Sünnikuupäev => new DateTime(Aasta, int.Parse(IK.Substring(3, 2)), int.Parse(IK.Substring(5, 2)));
        //{
        //    return new DateTime(Aasta(), int.Parse(IK.Substring(3, 2)), int.Parse(IK.Substring(5, 2)));
        //}

        int Aasta => ((IK[0] - 1) / 2 - 6) * 100 + int.Parse(IK.Substring(1, 2));

        public Sugu Sugu => (Sugu)(this.IK[0] % 2);
        //{
        //    return (Sugu)(this.IK[0] % 2);
        //}

        public static Sugu Gender(Inimene kes)
        {
            return (Sugu)(kes.IK[0] % 2);
        }


    }
}
