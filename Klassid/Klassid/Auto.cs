﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klassid
{
    // millest ma alustan, kui klassi tegema hakkan
    // 1. vaatan üle, mida mul vaja on - vb isegi kopipasten nõuded siia faili
    // 

    class Auto
    {
        // millest klass peab koosnema - iga asja puhul, mõtlen, mis tüüpi ta on
        public string Tootja; //      tootja
        public string Mudel;  //      mudel
        public int SilindriteArv = 4; //silindrite arv(vaikimisi võiks olla 4) - huvitav, mis tüüpi?
        public readonly string Numbrimärk; //numbrimärk - mida ei saa muuta
        // hm...

        // ToString - mis oskab joonistada auto stringiks
        // selleks teen funltsiooni ToString() - Henn ütles, et sinna käib sõna override - na ma usun
        public override string ToString()
         
        => $"{Tootja} auto {Mudel}. Tal on {SilindriteArv} {(SilindriteArv == 1 ? "silinder" : "silindrit")} ja ta on numbriga {Numbrimärk}";
        

        public Auto(string numbrimärk)
        {
            Numbrimärk = numbrimärk;
            Autod.Add(this);

            // ülikeeruline if-lause, mille eesmärgiks on vältida sama numbrimärgi korduvat lisamist
            // dictionary puhul nimelt ei tohi sama võtmega asja kaks korda lisada - saad vea
            // seda ülikeerulist if-lauset tuleb lugeneda nii
            //      vaatame, kas selline numbrimärk ON meil juba kirjas,
            //      kui ei ole, siis paneme selle uue auto dictionarisse kirja

            if (!AutodeDict.ContainsKey(numbrimärk)) AutodeDict.Add(numbrimärk, this);
            // me võime lisada siia miski hoiatuse - näiteks anda consoolile teada
            // else Console.WriteLine($"sellise numbriga {numbrimärk} auto meil juba on");
        }

        // klassis võiks olla üks nimekiri, kuhu kõik autod kirja pannakse

        // ok teeme hiljem - proovime kõigepealt - selleks läheme oma Testklassi (Program) testmeetodisse Main
        // appi see on eelmise ülesande sodi täis??? renameme laua puhtaks
        // kõik muu toimis, aga numbrit ei lase panna? mis ma tege ma pean
        // mida tuleb teha readonly muutujaga-väljaga? PEAN tegema konstruktori! 

        public static List<Auto> Autod = new List<Auto>();

        // klassis võiks olla veel üks nimekiri(dictionary)

        public static Dictionary<string, Auto> AutodeDict = new Dictionary<string, Auto>();

        // lihtsalt, et mul oleks kergem seletada, ma lisan siia kaks meetodit
        // ühe instantsi oma
        public void PrindiAuto1()
        {
            Console.WriteLine($"prindin auto {this.Numbrimärk}");
            Console.WriteLine(this.Mudel);
        }
        // teise staatilise
        public static void PrindiAuto2(Auto see)
        {
            Console.WriteLine($"prindin auto {see.Numbrimärk}");
            Console.WriteLine(see.Mudel);
        }

        // allpool veel mõned
        public Auto PrindiAuto3()
        {
            Console.WriteLine($"prindin auto {this.Numbrimärk}");
            Console.WriteLine(this.Mudel + " on " + SilindriteArv + " silindriga");
            return this;
        }

        public Auto LisaSilinder()
        {
            this.SilindriteArv++;
            return this;
        }

    }
}
