﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Klassid
{
    class Program
    {
        static void Main()
        {
            Auto hennuAuto =            // new tehte kirjeldus
                new
                    Auto        // klassi (andmetüübi) nimi
                    ("197DBB")  // parameetrid konstruktor-meetodile
                    { Tootja = "Toyota", Mudel = "Prius PlugIn" } // väljadele andtavad väärtused
                ;
            // trükime ühe auto
            //Console.WriteLine(hennuAuto);

            string failinimi = @"..\..\autod.txt";
            string[] failisisu = File.ReadAllLines(failinimi);
            foreach(var failirida in failisisu)
            {
                var reaosad = (failirida).Split(',');
                if (reaosad.Length == 4)
                new Auto(reaosad[3].Trim())
                {
                    Tootja = reaosad[0].Trim(),
                    Mudel = reaosad[1].Trim(),
                    SilindriteArv = int.Parse(reaosad[2].Trim())
                };
                else Console.WriteLine($"vigane rida {failirida}");
            }
            foreach (var x in Auto.Autod) Console.WriteLine(x);

            if (false)
            {
                Auto teine = new Auto("123XXX") { Tootja = "Nobe", Mudel = "A1", SilindriteArv = 0 };
                Auto kolmas = new Auto("123XXX") { Tootja = "Nobe", Mudel = "A2 Super", SilindriteArv = 0 };

                // trükime kõik autod
                foreach (var x in Auto.Autod) Console.WriteLine(x);

                // Eva Roos tahtis ühe lausega (see oli sinu küsimus, et kuidas saaks ühe korraga massiivi trükkida)
                Console.WriteLine(string.Join("\n", Auto.Autod));

                // nii saame dictionarist võtme järgi lugeda Auto
                Console.WriteLine(Auto.AutodeDict["197DBB"]);

                // trükime välja dictionary
                foreach (var x in Auto.AutodeDict) Console.WriteLine(x);

                // proovime neid kahte
                hennuAuto.PrindiAuto1();
                Auto.PrindiAuto2(hennuAuto);

                hennuAuto
                    .PrindiAuto3()
                    .LisaSilinder()
                    .PrindiAuto3()
                    ;

            }
        }

        // nimetan praegu (ajutiselt, et saaks kasutada) vana Main-meetodi ümber
        // ja teen uue - lihtsalt, et "laud oleks puhas"
        static void MainSodi(string[] args)
        {
            Console.WriteLine("Olen Ohoo Main.");
            foreach (var x in args) Console.WriteLine($"mulle anti {x}");

            

            Inimene i = new Inimene("36606066666") { Nimi = "Ants" };
            Console.WriteLine(i.Palk);  // instantsi väli - läbi muutuja.

            Inimene.MiinimumPalk = 800; // static väli - läbi klassinimi.
            Console.WriteLine((new Inimene("37707077777") { Nimi = "Peeter" }).Palk);


            Inimene uusNägu;  // hetkel muutujal ei ole väärtust
            uusNägu = null;   // muutuja väärtus on kandiline null

            // kolm erinevat võimalust teha sama asi, luua uus objekt ja anda tema väljadele väärtused
            uusNägu = new Inimene("38808088888");
            uusNägu.Nimi = "Joosep";

            uusNägu = new Inimene("39909099999") { Nimi = "Karla" };

            // järgnev formaat NÕUAB, et oleks ilma parameetriteta konstruktor
            uusNägu = new Inimene { Nimi = "Feeliks" };

            foreach (var x in Inimene.Inimesed)
            {
                Console.WriteLine(x);
            }


        }
    }

    public class Inimene
    {
        // teeme staatilise muutuja - inimeste list, kuhu kõik inimesed kirja pannakse
        public static List<Inimene> Inimesed = new List<Inimene>();

        static int inimesi = 0; // private static

        static decimal miinimumPalk = 400;
        internal static decimal MiinimumPalk  // miinimumpalka saab muuta vaid mooduli (projekti) seest
        {
            private get => miinimumPalk; // aga näha (get) saab ainult selle klassi meetodites
            set => miinimumPalk = Math.Max(miinimumPalk, value);
        }

        int number = ++inimesi; // private instantse
        public readonly string IK; // isikukood

        public string Nimi = "nimeta"; // näha kõigis (ka võõrastes) projektides
        internal int Vanus; // näha ainult selles moodulis (projektis)

        private decimal palk = miinimumPalk;
        public decimal Palk
        {
            get => palk;
            set => palk = value > palk ? value : palk;
        }

        // siia kohta kirjutan nüüd uue asja nimega konstruktor
        // konstruktor on klassiga sama nimega meetod, mis kutsutakse automaatselt
        // kui täidetakse new korraldust

            // kaks konstruktorit - overloadimine
            // optional parameeter võimaldab overloadimist vältida (peita)
        public Inimene(string ik)
        {
            this.IK = ik;
            Inimesed.Add(this);
        }

        public Inimene() : this("00000000000")
        { }

        public override string ToString() => $"{number}. {Nimi} (isikukood {IK})";


        

    }

   
}
