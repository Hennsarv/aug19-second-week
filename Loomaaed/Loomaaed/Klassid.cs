﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loomaaed
{

    class Loom // baasklass (millest on tuletatud mõni teine)
    {
        public string Liik;

        public Loom(string liik = "tundmatu") => Liik = liik; 

        // meetod, mida saab tuletatud klassis ümder mõelda (defineerida)
        public virtual void TeeHäält() => Console.WriteLine($"{Liik} teeb koledat häält");

        public override string ToString() => $"on loom liigist {Liik}";
    }

    class Koduloom : Loom // baasklassist Loom tuletatud klass
        // temal on KÕIK baasklassi omadused ja funktsioonid (peale konstruktori)
    {
        public string Nimi;   // lisatud väli - seda baasklassil ei ole

        // tuletatud klassi konstruktor võib kasutada baasklassi oma
        public Koduloom(string liik, string nimi = "nimeveelpole") : base(liik)
        { Nimi = nimi; }

        public override string ToString() => $"{Nimi} on {Liik}";

        // baasklassi meetod on ümber defineeritud
        public override void TeeHäält()
        {
            Console.WriteLine($"{Nimi} teeb mahedat häält (ta on {Liik})");
        }

        // lisame veel ühe meetodi - aga seekorda ilma kehata
        

    }

    class Kass : Koduloom
    {
        public string Tõug = "segavereline";
        public Kass(string nimi) : base("kass", nimi) { }

        bool tuju = false;

        public void SikutaSabast() => tuju = false;
        public void Silita() => tuju = true;

        public override void TeeHäält()
        {
            if (tuju) Console.WriteLine($"{Tõug} kass {Nimi} lööb mõnusasti nurru");
            else Console.WriteLine($"{Nimi} kräunub");
        }

    }

    class Koer : Koduloom
    {
        public string Tõug = "krants";
        public Koer(string nimi) : base("koer", nimi) { }
        public override void TeeHäält() => Console.WriteLine($"{Tõug} {Nimi} haugub");
        
    }

}
