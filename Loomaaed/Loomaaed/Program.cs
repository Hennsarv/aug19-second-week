﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loomaaed
{
    class Program
    {
        static void Main(string[] args)
        {
            Loom kroko = new Loom("krokodill");
            Console.WriteLine(kroko);
            kroko.TeeHäält();
            Koduloom kodune = new Koduloom("prussakas");
            kodune.Nimi = "Pati";
            Console.WriteLine(kodune);
            kodune.TeeHäält();

            Console.WriteLine("\nmõned asjad veel\n");
            List<Loom> loomad = new List<Loom>();
            loomad.Add(kroko);
            loomad.Add(kodune);

            foreach (var x in loomad) Console.WriteLine(x);

            Koer k1 = new Koer("Pontu") { Tõug = "taks"};
            k1.TeeHäält();

            Kass k2 = new Kass("Miisu") { Tõug = "angoora" };
            k2.Silita();
            k2.TeeHäält();
            
        }
    }

}
