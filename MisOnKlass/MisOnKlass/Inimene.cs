﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnKlass
{
    class Inimene
    {
        private string eesnimi;
        private string perenimi;
        public DateTime Sünniaeg;
        // hetkel mul muud siin klassis ei ole - alustasin nullist
        // küll me kohe lisame siia midagi

        // ära järele tee praegu - külll sa jõuad

        private decimal palk; // see on pesa, kus hoitakse selle välja väärtust
        public decimal Palk
        {
            get => palk; // see on funktsioon, mis selle välja väärtust NÄITAB (get)
            set => palk = value > palk ? value : palk; 
            // set on meetod, mis meie kontrolli all lubab sellele väljale uusi väärtusi anda
            //set
            //{
            //    if (value > palk) palk = value;
            //}
        }


        public string GetEesnimi() => eesnimi;  // getter-funktsioon
        public void SetEesnimi(string nimi) => eesnimi = TitleCase(nimi); // setter-meetid

        public string Eesnimi //property kirjutatud lühidalt
        {

            get => eesnimi;  // seesama getterfunktsioon
            set => eesnimi = TitleCase(value);   // seesama settermeetid
        }
        public string Perenimi //property kirjutatud pikemalt
        {

            get { return perenimi; } // seesama getterfunktsioon
            set { perenimi = TitleCase(value); }  // seesama settermeetid
        }

        public string Täisnimi => eesnimi + " " + perenimi; // r/o property-ga

        public int Vanus => (DateTime.Today - Sünniaeg).Days * 4 / 1461; // r/o property

        public int Vanus1 // sama asi pikemalt
        {
            get => (DateTime.Today - Sünniaeg).Days * 4 / 1461;
        }

        // sama asi funktsioonina
        public int Vanus2() => (DateTime.Today - Sünniaeg).Days * 4 / 1461;
        

        public void Prindi() =>
        
            Console.WriteLine($"On inimene, kelle nimi {Eesnimi} {Perenimi}");
        
        public static string TitleCase(string nimi)
        {
            string[] nimed = nimi.Split(' ');
            for (int i = 0; i < nimed.Length; i++)
            {
                nimed[i] = nimed[i].Length == 0 ? "" :
                    nimed[i].Substring(0, 1).ToUpper() + nimed[i].Substring(1).ToLower();
            }
            return string.Join(" ", nimed);
        }

    }
}
