﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MisOnKlass
{
    class Program
    {
        static void Main(string[] args)
        {
            Inimene henn = new Inimene
            {
                //Eesnimi = "henn",
                Perenimi = "sarv",
                //Sünniaeg = DateTime.Parse("1955/03/07")
                Sünniaeg = new DateTime(1955, 03, 07)
            };
            henn.SetEesnimi("sarviktaat");
            henn.Eesnimi = "sarviktaat";

            Console.WriteLine($"1. {henn.Palk}"); // mis trükitakse siin
            henn.Palk = 10000;
            Console.WriteLine($"2. {henn.Palk}"); // siin?
            henn.Palk = 4000;
            Console.WriteLine($"3. {henn.Palk}"); // siin
            



            henn.Prindi();  // meetodi Prindi() väljakutse

            Console.WriteLine(henn.Vanus1); // funktsiooni (Vanus()) kasutamine avaldises
            Console.WriteLine(henn.Vanus2()); // funktsiooni (Vanus()) kasutamine avaldises

            // ma teen siia natuke lihtsamat juttu
            // meetoditest ja funktsioonidest

            // funktsiooni poole pöördumisel antakse ette argument
            // argument on väärtus, mis omistatakse parameetrile
            Console.WriteLine(   Summa(1,2,3,4)  );
            int[] arvud = { 1, 2, 7 };
            Console.WriteLine(Summa(arvud));

            int x = 4;
            Imelik(ref x);
            Console.WriteLine(x);

            int a = 3; int b = 7;
            Console.WriteLine($"a = {a}, b = {b}");
            Swap(ref a, ref b);
            Console.WriteLine($"a = {a}, b = {b}");

            string sa = "Ants";
            string sp = "Peeter";
            Console.WriteLine($"sa = {sa}, sp = {sp}");
            Swap(ref sa, ref sp);
            Console.WriteLine($"sa = {sa}, sp = {sp}");

        }
        static int Liida(int x, int y, int z = 7)
        {
            return x + y + z;
        }
        // overload, overloaded funktsioon
        // sama nimega erinevate parameetritega
        static int Liida(short x, int t)
        {
            return x + t;
        }

        static int Summa(int x) => x;
        //static int Summa(int x, int y) => x + y;
        //static int Summa(int x, int y, int z) => x + y + z;
        static int Summa(params int[] arvud)
        {
            int summa = 0; foreach (var x in arvud) summa += x;
            return summa;
        }

        static void Imelik(ref int miskiArv)
        {
            miskiArv += 7;
        }

        static void Vaheta(ref int yks, ref int teine)
        {
            int ajutine = yks;
            yks = teine;
            teine = ajutine;
        }
        // geneeriline funktsioon/meetod, T tähistab paramneetri tüüpi
        static void Swap<T>(ref T yks, ref T teine)
        {
            T ajutine = yks;
            yks = teine;
            teine = ajutine;
        }

        static int Factorial(int x)
        {
            if (x < 2) return x;
            return x * Factorial(x - 1);
        }

    }

   
}
