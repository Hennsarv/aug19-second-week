﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SoojendusHarjutus
{
    class Program
    {
        static void Main(string[] args)
        {
            // soojendusharjutus - Henn, palun pane salvestama
            // salvestab juba

            int ridu = 8;
            int veerge = 10;
            int[,] arvud = new int[ridu, veerge];

            Random r = new Random();

            // kõigepealt täidame massiivi arvudega
            for (int i = 0; i < ridu; i++)
                for (int j = 0; j < veerge; j++)
                {
                    arvud[i, j] = r.Next(100) + 1;
                }

            // seejärel prindime ekraanile välja
            for (int i = 0; i < ridu; i++)
            {
                for (int j = 0; j < veerge; j++)
                {
                    Console.Write($"{arvud[i, j]}\t");
                }
                Console.WriteLine();
            }

            // nüüd oleks vaja otsimisega tegelda
            // mida otsida -  küsime

            ;

            for (bool kasKüsimeEdasi = true; kasKüsimeEdasi;)
            {
                Console.Write("mida otsime: ");
                if (int.TryParse(Console.ReadLine(), out int otsitav) && otsitav <= 100 && otsitav > 0)
                {
                    // siin on muutujas otsitav kindlasti ARV
                    // teeme, mis meil vaja teha
                    int mituLeidsin = 0;
                    bool leidsin = false;
                    for (int i = 0; i < ridu && !leidsin; i++)
                        for (int j = 0; j < veerge && !leidsin; j++)
                        {
                            if (arvud[i, j] == otsitav)
                            {
                                Console.WriteLine($"leidsin {otsitav} {i + 1}-reast ja {j + 1}-veerust");
                                // ma peaks teada andma, et ma vähemalt 1 leidsin
                                mituLeidsin++;
                            }
                        }
                    // jõudsime lõppu välja - mis siin on meil teada
                    // muutuja kasÜldseLeidsin on false või true?
                    if (mituLeidsin == 0) Console.WriteLine("ma ei leidnud ühtegi");
                    else Console.WriteLine($"kokku leidsin {mituLeidsin} sellist");


                    kasKüsimeEdasi = false;
                }
                else
                {
                    Console.WriteLine("see pole miskine arv või pole ta vahemikus 1..100");
                }

            }
        }
    }
}
