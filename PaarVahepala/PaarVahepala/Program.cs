﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Henn.Asjad
{

    class Program
    {
        static void Main(string[] args)
        {
            Mast m = Mast.Poti;
            m--;
            Console.WriteLine(m);

            Tunnus t = (Tunnus)10;
            t |= Tunnus.Puust   ;
            Console.WriteLine(t);

            // Henn unustas teile rääkida, mis asi on casting

            decimal d = 10;
            int i = (int)(d+7);

            // tüübiteisendus - kolme moodi
            // 1. Parse või TryParse funktsioon
            //  sihttüübi klassis on vastav funktsioon
            for (string vastus = "*"; vastus != "";)
            {
                Console.Write("ütle üks kuupäev: ");
                
                if (DateTime.TryParse(vastus = Console.ReadLine(), out DateTime dt))
                {
                    Console.WriteLine(dt);
                }
                else Console.WriteLine("see pole kuupäev");
            }
            // 2. Convert klassis on ToType funktsioone terve pesu

            // 3. SOBIVATE tüüpide puhul castingu tehe
            //  (sihttüüp)(lähtetüüpi-avaldis)


            // see jutt siin läks kätte ära ümmardamiseks, mis ei olnud selle jutu teema
            double x = 4.7;
            double y = 0.7;

            Console.WriteLine((int)Math.Round(x + y + 0.49999999999999));
            // Math.Floor ja Math.Ceiling on rounddown ja roundup vasted matemaatikutele :)

            Inimene henn = new Inimene
            {
                Nimi = "Henn Sarv",
                Sünniaeg = new DateTime(1955, 3, 7),
                Sugu = Sugu.Mees
            };

            MessageBox.Show
                (
                "Tere Henn!", 
                "tervitus", 
                MessageBoxButtons.AbortRetryIgnore, 
                MessageBoxIcon.Stop);
        }
    }

    enum Mast { Risti = 1, Ruutu, Ärtu, Poti, Pada = 4 }

    [Flags]
    enum Tunnus { Suur = 1, Punane = 2, Puust = 4, Kolisev = 8}

    enum Sugu { Naine, Mees}

    class Inimene
    {
        public string Nimi;
        public DateTime Sünniaeg;
        public Sugu Sugu;
    }



}
