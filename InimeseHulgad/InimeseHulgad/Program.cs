﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InimeseHulgad
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Inimene> õpilased = System.IO.File.ReadAllLines(@"..\..\õpilased.txt")
                .Select(x => x.Split(' ').Select(y => y.Trim()).ToArray())
                .Select(x => new Inimene { Eesnimi = x[1], Perenimi = x[2], IK = x[0] })
                .ToList()
                ;
            List<Inimene> õpetajad = System.IO.File.ReadAllLines(@"..\..\õpetajad.txt")
                .Select(x => x.Split(' ').Select(y => y.Trim()).ToArray())
                .Select(x => new Inimene { Eesnimi = x[1], Perenimi = x[2], IK = x[0] })
                .ToList()
                            ;
            var inimesed = õpilased.Union(õpetajad);
            var inimesteDictionary = inimesed.ToDictionary(x => x.IK);

            var grupByMonth = inimesed.ToLookup(x => x.Sündinud.DayOfWeek);
            foreach (var x in grupByMonth[DayOfWeek.Saturday]) Console.WriteLine(x);

            foreach (var x in grupByMonth) Console.WriteLine($"{x.Key} {x.Count()}");

            //foreach (var x in inimesed.OrderBy(x => x.Eesnimi)) Console.WriteLine(x);

            õpilased.ForEach(x => Console.WriteLine(x) );

            int[] arvud = { 1, 3, 5, 23, 4, 8, 5, 2 };
            foreach (var x in arvud.Select(x => new { arv = x, ruut = x * x})) Console.WriteLine(x);

            var selected1 = arvud.Select(x => { if (x % 2 == 0) return x * x; else return x * x * x; });
            // näed täitsa lubatud ka nii!
            var selected2 = arvud.Select(x => x % 2 == 0 ? x * x : x * x * x);
            // enamvähem sama asi

            var miski = new { Nimi = "Henn", Vanus = 64 };

            

        }
    }

    class Inimene
    {
        public string Eesnimi;
        public string Perenimi;
        public string IK;
        public DateTime Sündinud => new DateTime
        (
            ((IK[0]-1) / 2 - 6) * 100 + int.Parse(IK.Substring(1, 2)),
            int.Parse(IK.Substring(3, 2)),
            int.Parse(IK.Substring(5, 2))
         );
        public int Vanus => (DateTime.Today - Sündinud).Days * 4 / 1461;

        public override string ToString() => $"{Eesnimi} {Perenimi} ({Vanus} aastane)";
    }
}
